<!----------------------->
<!--@ Made By Jay \o/ @-->
<!----------------------->

# SystemBreak Website
This is the source code for [SystemBreak](https://systembreak.ca/)


## Quick Setup

1. Install Docker and Docker-Compose

- [Docker Install documentation](https://docs.docker.com/install/)
- [Docker-Compose Install documentation](https://docs.docker.com/compose/install/)

2. Setup server with proper Port Forwarding

    A Helpful script is included and can be run with 
```bash
./setup.sh
```

3. Bring the stack up using:

```bash
docker compose up -d
```

4. Bring the stack down using:

```bash
docker compose down
```

## Roadmap 
- [x] Working virtualization 
- [x] Custom Portforwarding on port 80
- [x] Link gitlab repo on website 
- [x] Show off 
- [x] Push to a live server 
- [ ] Beautify Website 
- [x] Add Subpages 
- [ ] Beautify Subpages 
- [x] Live website update 
- [x] Integrate Secure Login for live server output/resource monitor 

## Authors and acknowledgment
Dev - Jacob Chotenovsky
#### THANKS TO
[Certbot](https://certbot.eff.org/)   
[Nginx](https://nginx.org/en/)   
[NodeJS](https://nodejs.org/en/)   
[Stackoverflow](https://stackoverflow.com/)   

## Project status
Like 40%
