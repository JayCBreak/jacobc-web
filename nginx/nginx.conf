events {
    # worker_connections  1024;
}

http {
    # Setup
    server_tokens off;
    charset utf-8;

    # HTTP
    # Redirects to the HTTPS version of this website
    server {
        # Listens on the HTTP port
        listen 80 default_server;

        server_name _;
        # 301 is a permenant redirect to the HTTPS website
        return 301 https://$host$request_uri;
    }

    # HTTPS
    server {
        # Listens on the HTTPS port using SSL and HTTP/2
        listen 443 ssl http2;
        # Use the certificates
        ssl_certificate     /etc/letsencrypt/live/systembreak.ca/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/systembreak.ca/privkey.pem;
        # Setup the website settings
        server_name systembreak.ca www.systembreak.ca;
        root /var/www/html;
        index index.php index.html index.htm;

        # Redirect to the docker container with the site running
        location / {
            proxy_pass http://site:5000/;
        }
        # Setup ACME challenge for Certbot
        location ~ /.well-known/acme-challenge/ {
            root /var/www/certbot;
        }
    }
}