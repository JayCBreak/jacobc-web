/*********************\ 
*@  Made By Jay \o/  @*
\*********************/

// Get libraries
const path = require('path')
const express = require('express')

// Setup Variables
const app = express()
const port = 5000
const publicDirPath = path.join(__dirname, '../app/public')


// Redirect to html of the page requested
app.use(express.static(publicDirPath, {extensions: ['html']}))
// If the requested page does not exist, send 404.html
app.use(function (req, res) {
    res.status(404).sendFile(publicDirPath + '/404.html')
})

// Listening to the port for HTTP requests
app.listen(port, () => {
    console.log('Server is up and running on PORT ${port}.')
})
