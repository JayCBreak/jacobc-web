#!/bin/bash

#######################
#@  Made By Jay \o/  @#
#######################

sudo firewall-cmd --permanent --zone=public --add-port=80/tcp
sudo firewall-cmd --permanent --zone=public --add-port=443/tcp
sudo firewall-cmd --reload
